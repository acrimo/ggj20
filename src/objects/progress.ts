import { GameScene } from "../scenes/game-scene"
import { Game } from "phaser";

const MAX_MODULES = 10;

export class Progress {
    private scene: GameScene;

    public bar: Phaser.GameObjects.Image;
    public back: Phaser.GameObjects.Image;

    private advance: number;

    constructor(scene: GameScene) {
        this.scene = scene;

        this.back = scene.add.image(540, 90, "progress-back");

        this.bar = scene.add.image(135, 90, "barra")
            .setOrigin(0, 0.5).setScale(0, 1);

        this.advance = 0;
    }

    public update(delta: number): void {
        this.advance += delta * 0.25;
        let scale = this.advance / (MAX_MODULES * 1920);

        if (scale > 1) {
            scale = 1;
            this.scene.finish();
        }

        this.bar.setScale(scale, 1);
    }
}
/// <reference path='../phaser.d.ts'/>

import { GameScene, GameState } from "../scenes/game-scene";

class Flame extends Phaser.GameObjects.Sprite {
    public offsetX: number = 46;
    public offsetY: number = 30;
    public isR: boolean;
    public constructor(scene: GameScene, _x: number, _y: number, _isR: boolean, texture: string) {
        super(scene, _x, _y, texture);
        this.isR = _isR;
        this.setScale(0.22, 0.22);
        this.setOrigin(0.5 + (this.isR ? -this.offsetX : this.offsetX) / (this.width * 0.22), 0.5 + this.offsetY / (this.height * 0.22)).setVisible(false);
        scene.add.existing(this);
    }

    public updatePosition(_x: number, _y: number) {
        this.setPosition(_x, _y);
    }
}

export class Ship extends Phaser.GameObjects.Sprite {

    public vx: number;
    public vy: number;
    private titaR: number;
    private titaL: number;
    private isRPressed: boolean = false;
    private isLPressed: boolean = false;

    private flameL: Flame;
    private flameR: Flame;

    private HPx: number = 5;
    private HPy: number = 2;
    private ngFriction: number = 1.5;
    private twist: number = 5;
    private limitWheel: number = 130;
    private rotationFactor: number = 1.5;

    public constructor(scene: GameScene) {
        super(scene, 540, 1600, "ship");
        this.setOrigin(0.5, 1);
        scene.add.existing(this);

        this.flameR = new Flame(scene, this.x, this.y, true, 'f0');
        this.flameL = new Flame(scene, this.x, this.y, false, 'f0');
        this.init();
    }

    private init() {
        this.titaR = 0;
        this.titaL = 0;
        this.vx = 0;
        this.vy = 0;
    }

    public LeftTurbineOn() {
        this.isLPressed = true;
        this.flameL.setVisible(true).play('flames');
        // this.flameL.setVisible(true);
    }

    public LeftTurbineOff() {
        this.isLPressed = false;
        this.flameL.setVisible(false);
    }

    public RightTurbineOn() {
        this.isRPressed = true;
        this.flameR.setVisible(true).play('flames');
        // this.flameL.setVisible(true);
    }

    public RightTurbineOff() {
        this.isRPressed = false;
        this.flameR.setVisible(false);
    }

    private updateTitaR() {
        this.titaR += - this.ngFriction + (this.isRPressed ? this.twist : 0);
        if (this.titaR < 0) this.titaR = 0;
        if (this.titaR >= this.limitWheel) this.titaR = this.limitWheel;
    }

    private updateTitaL() {
        this.titaL += - this.ngFriction + (this.isLPressed ? this.twist : 0);
        if (this.titaL < 0) this.titaL = 0;
        if (this.titaL >= this.limitWheel) this.titaL = this.limitWheel;
    }

    private powerRightTurbine() {
        let x0 = Math.cos(this.titaR * Math.PI / 180);
        let y0 = - Math.sin(this.titaR * Math.PI / 180);
        this.updateTitaR();
        let x1 = Math.cos(this.titaR * Math.PI / 180);
        let y1 = - Math.sin(this.titaR * Math.PI / 180);

        let dx = x1 - x0;
        let dy = y1 - y0;

        this.vx += this.HPx * dx;
        if (dy < 0 && this.vy > 0) this.vy = 0;
        this.vy += this.HPy * dy;
    }

    private powerLeftTurbine() {
        let x0 = - Math.cos(this.titaL * Math.PI / 180);
        let y0 = - Math.sin(this.titaL * Math.PI / 180);
        this.updateTitaL();
        let x1 = - Math.cos(this.titaL * Math.PI / 180);
        let y1 = - Math.sin(this.titaL * Math.PI / 180);

        let dx = x1 - x0;
        let dy = y1 - y0;

        this.vx += this.HPx * dx;
        if (dy < 0 && this.vy > 0) this.vy = 0;
        this.vy += this.HPy * dy;
    }

    private doRotation() {
        var _new: number;
        _new = (this.titaL - this.titaR) / this.rotationFactor;

        this.rotation = _new * Math.PI / 180;
        if (this.rotation >= this.limitWheel * Math.PI / 180) this.rotation = this.limitWheel * Math.PI / 180;
        if (this.rotation <= -this.limitWheel * Math.PI / 180) this.rotation = -this.limitWheel * Math.PI / 180;
        // console.log((this.rotation * 180 / Math.PI).toFixed(4), (this.titaL - this.titaR).toFixed(4));

        this.flameR.setRotation(this.rotation);
        this.flameL.setRotation(this.rotation);
    }

    private move(delta: number) {
        this.x = this.x + this.vx * delta;
        this.y = this.y + this.vy * delta;
        this.flameR.updatePosition(this.x, this.y);
        this.flameL.updatePosition(this.x, this.y);
    }

    public update(delta: number) {
        this.powerRightTurbine();
        this.powerLeftTurbine();
        this.doRotation();
        this.move(delta);
        // console.log(this.x.toFixed(4), this.y.toFixed(4), this.vx.toFixed(4), this.vy.toFixed(4), this.titaL.toFixed(4), this.titaR.toFixed(4));
    }
}
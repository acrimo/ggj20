import { GameScene } from "../scenes/game-scene"

export class Lifebar {
    private scene: GameScene;

    public lifes: Array<Phaser.GameObjects.Image>;
    public life: number = 3;

    constructor(scene: GameScene) {
        this.scene = scene;

        this.lifes = new Array<Phaser.GameObjects.Image>(3);
        for (let i = 0; i < 3; i++) {
            this.lifes[i] = scene.add.image(385 + i * 155, 180, "life-back").setTint(0x00FF36);
        }
    }

    public takeHit(): boolean {
        this.life--;

        this.lifes[this.life].setTint(0x0000FF);
        return this.life == 0;
    }
}
import { GameScene } from '../scenes/game-scene';
import { Tilemaps } from 'phaser';

// const STEPS = 200;
const MAX_MODULES = 10;

export class Pipe {
    private scene: GameScene;

    // private leftPoints: Array<number>;
    // private rightPoints: Array<number>;
    private backgrounds: Array<Phaser.GameObjects.Image>;
    private index: Array<number>;

    private debug: Phaser.GameObjects.Graphics;
    // private polygon: Phaser.Geom.Polygon;

    // private ship: Phaser.GameObjects.Graphics;
    // private alpha: Phaser.GameObjects.Image;

    private moduleCount: number;

    private final: number = -1;

    constructor(scene: GameScene) {
        this.scene = scene;

        this.backgrounds = new Array<Phaser.GameObjects.Image>(3);
        this.index = new Array<number>(3);

        // for (let i = 0; i < 3; i++) {
        //     this.backgrounds[i] =
        //         this.scene.add.image(0, -1920 * i, "pipe-" + i)
        //             .setOrigin(0);

        //     this.index[i] = i;
        // }

        for (let i = 0; i < 2; i++) {
            this.backgrounds[i] =
                this.scene.add.image(0, -1920 * i, "pipe-" + i)
                    .setOrigin(0);

            this.index[i] = i;
        }

        this.backgrounds[2] = this.scene.add.image(0, -1920 * 2, "pipe-0")
            .setOrigin(0);
        this.index[2] = 2;

        this.debug = this.scene.add.graphics();

        // this.alpha = this.scene.add.image(0, 0, "pipe-alpha-1").setOrigin(0);
        // this.ship = this.scene.add.graphics();

        // this.path = game.add.graphics();
        // this.path.defaultFillColor = 0xffffff;
        // this.path.defaultStrokeWidth = 20;
        // this.path.defaultStrokeColor = 0xff0000;

        // this.leftPoints = Array<number>();
        // this.rightPoints = Array<number>();

        // for (let i = 0; i < STEPS - 1; i++) {
        //     this.leftPoints.push(440, 3000 - i);
        //     this.rightPoints.push(640, 3000 - i);
        // }

        // this.leftPoints.push(250, 1920);
        // this.rightPoints.push(750, 1920);

        this.moduleCount = 3;
    }

    public update(delta: number, x: number, y: number, rot: number): boolean {
        // mouse check
        // this.ship.clear();

        // let x = this.scene.input.mousePointer.position.x;
        // let y = this.scene.input.mousePointer.position.y;

        // if (this.scene.textures.getPixel(x, y, "pipe-alpha-1", 0).alpha == 255)
        //     this.ship.fillStyle(0xffffff);
        // else
        //     this.ship.fillStyle(0xff0000);


        // this.ship.fillCircle(this.scene.input.mousePointer.position.x,
        //     this.scene.input.mousePointer.position.y,
        //     50);

        if (this.final == -1)
            for (let i = 0; i < 3; i++) {
                if (this.backgrounds[i].y > 1920) {
                    if (i == 0)
                        this.backgrounds[0].y = this.backgrounds[2].y - 1920;
                    else
                        this.backgrounds[i].y = this.backgrounds[i - 1].y - 1920;

                    if (this.moduleCount < MAX_MODULES) {
                        this.index[i] = Math.floor(Math.random() * 3.99);
                        this.moduleCount++;

                        // mirror lanes
                        if (this.index[i] > 1) {
                            this.backgrounds[i].setTexture("pipe-" + (this.index[i] - 2));
                            this.backgrounds[i].flipX = true;
                        } else {
                            this.backgrounds[i].setTexture("pipe-" + this.index[i]);
                            this.backgrounds[i].flipX = false;
                        }

                        // // multi lanes
                        // this.backgrounds[i].setTexture("pipe-" + this.index[i]);
                    } else {
                        this.backgrounds[i].setTexture("end");
                        this.backgrounds[i].flipX = false;
                        this.final = i;
                    }
                }
            }

        if (this.final != -1) {
            if (this.backgrounds[this.final].y >= 0) {
                this.backgrounds[this.final].y = 0;
            } else {
                for (let i = 0; i < 3; i++)
                    this.backgrounds[i].y += delta * 0.25;
            }
        } else {
            for (let i = 0; i < 3; i++)
                this.backgrounds[i].y += delta * 0.25;
        }

        // step system
        // for (let i = 0; i < STEPS; i++) {
        //     this.leftPoints[i * 2 + 1] += delta * 1;
        //     this.rightPoints[i * 2 + 1] += delta * 1;
        // }

        // this.draw();
        // this.generate();

        let x0 = Math.round(x + 62.5 * Math.sin(rot * 0.0174533));
        let y0 = Math.round(y - 62.5 * Math.cos(-rot * 0.0174533));

        let x1 = Math.round(x + 150 * Math.sin(rot * 0.0174533));
        let y1 = Math.round(y - 150 * Math.cos(-rot * 0.0174533));

        // // debug circles for collision
        // this.debug.clear();
        // this.debug.fillStyle(0x00ff00, 0.6);
        // this.debug.fillCircle(x0, y0, 62.5);
        // this.debug.fillCircle(x1, y1, 62.5);

        for (let i = 0; i < 3; i++) {
            if (y0 > this.backgrounds[i].y && y0 < this.backgrounds[i].y + 1920) {

                if (x0 < 0 || x0 > 1080 || x1 < 0 || x1 > 1080) return false;

                if (this.index[i] > 1) {
                    if (this.scene.textures.getPixel(1080 - x0, y0 - this.backgrounds[i].y,
                        "pipe-alpha-" + (this.index[i] - 2), 0).alpha != 255)
                        return false;
                } else {
                    if (this.scene.textures.getPixel(x0, y0 - this.backgrounds[i].y,
                        "pipe-alpha-" + this.index[i], 0).alpha != 255)
                        return false;
                }
            }

            if (y1 > this.backgrounds[i].y && y1 < this.backgrounds[i].y + 1920) {
                if (this.index[i] > 1) {
                    if (this.scene.textures.getPixel(1080 - x1, y1 - this.backgrounds[i].y,
                        "pipe-alpha-" + (this.index[i] - 2), 0).alpha != 255)
                        return false;
                } else {
                    if (this.scene.textures.getPixel(x0, y1 - this.backgrounds[i].y,
                        "pipe-alpha-" + this.index[i], 0).alpha != 255)
                        return false;
                }
            }
        }

        return true;
    }

    // private draw() {
    //     this.path.clear();
    //     this.path.beginPath();

    //     this.path.moveTo(this.leftPoints[0], this.leftPoints[1]);
    //     for (let i = 1; i < STEPS; i++) {
    //         this.path.lineTo(this.leftPoints[i * 2], this.leftPoints[i * 2 + 1]);
    //     }

    //     for (let i = STEPS - 1; i >= 0; i--) {
    //         this.path.lineTo(this.rightPoints[i * 2], this.rightPoints[i * 2 + 1]);
    //     }

    //     this.path.closePath();
    //     this.path.fillPath();
    //     this.path.strokePath();
    // }

    // private lx: number = 250;
    // private rx: number = 750;

    // private dx: number = 0;

    // private generate(): void {
    //     if (this.rightPoints[1] > 3000) {
    //         this.rightPoints.shift(); this.rightPoints.shift();
    //         this.leftPoints.shift(); this.leftPoints.shift();

    //         let ry = this.rightPoints[this.rightPoints.length - 1] - 30;
    //         let ly = this.leftPoints[this.leftPoints.length - 1] - 30;

    //         // let disp = 20 * (Math.random() * 2 - 1);

    //         if (this.dx < 20 && this.dx > -20)
    //             this.dx += 2 * (Math.random() * 2 - 1);
    //         else {
    //             let aux = 20 * (Math.random() * 10 + ((this.dx >= 20) ? -8 : -2));
    //             this.dx += aux;
    //         }

    //         let disp = this.dx;

    //         let rx = this.rightPoints[this.rightPoints.length - 2];
    //         let lx = this.leftPoints[this.rightPoints.length - 2];

    //         if (rx + disp > 1080 || lx + disp < 0) {
    //             // disp = -disp;
    //             // this.dx = -this.dx
    //         }

    //         rx += disp; lx += disp;
    //         rx += disp; lx += disp;

    //         let rd = + 50 * (Math.random() * 2 - 1);

    //         this.rightPoints.push(rx); this.rightPoints.push(ry);
    //         this.leftPoints.push(lx); this.leftPoints.push(ly);
    //     }
    // }
}
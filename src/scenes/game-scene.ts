/// <reference path='../phaser.d.ts'/>

import { Ship } from "../objects/ship";
import { Pipe } from "../objects/pipe";
import { Progress } from "../objects/progress";
import { Lifebar } from "../objects/lifebar";
import { Game } from "phaser";

export enum GameState {
    INIT,
    PLAY,
    FINISH,
    LOOSE
}

export class GameScene extends Phaser.Scene {
    // state machine
    public state: GameState;

    // ship
    private ship: Ship;
    private pipe: Pipe;

    private progress: Progress;
    private lifebar: Lifebar;

    private gravity: number;

    private keyD: Phaser.Input.Keyboard.Key;
    private keyA: Phaser.Input.Keyboard.Key;

    private inmune: boolean = false;

    constructor() {
        super({
            key: "GameScene"
        });
    }

    create(): void {
        this.pipe = new Pipe(this);
        this.ship = new Ship(this);

        this.progress = new Progress(this);
        this.lifebar = new Lifebar(this);

        this.gravity = .05;
        this.gravity = .01;

        this.keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        this.keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

        this.cameras.main.fadeIn(500);
        this.state = GameState.INIT;
        this.loadTutorial();
    }

    public update(time: number, delta: number): void {
        // delta *= 4;

        switch (this.state) {
            case GameState.PLAY:
                this.progress.update(delta);

                // all this shit
                if (this.keyD.isDown) { this.ship.RightTurbineOn(); }
                else { this.ship.RightTurbineOff(); }
                if (this.keyA.isDown) { this.ship.LeftTurbineOn(); }
                else { this.ship.LeftTurbineOff(); }

                this.ship.update(delta / 16);
                this.ship.vy += this.gravity * (delta / 16);

                if (this.ship.x < 0) this.ship.x = 0;
                if (this.ship.x > 1080) this.ship.x = 1080;

                let inside = this.pipe.update(
                    delta, this.ship.x, this.ship.y, this.ship.angle);

                if (!inside && !this.inmune) {
                    if (!this.lifebar.takeHit()) {
                        this.inmune = true;
                        this.tweens.add({
                            targets: this.ship,
                            alpha: 0.2,
                            yoyo: true,
                            repeat: 5,
                            duration: 200,
                            onComplete: () => {
                                this.inmune = false;
                            }
                        });
                    }
                    else {
                        this.loose();
                    }
                }
        }

    }

    public finish() {
        this.state = GameState.FINISH;

        this.tweens.add({
            targets: this.ship,
            duration: 3000,
            rotation: 0,
            x: 800,
            y: 1600,
            ease: "Cubic.easeOut",
            onComplete: () => {
                this.tweens.add({
                    targets: [this.progress.back, this.progress.bar, this.lifebar.lifes[0], this.lifebar.lifes[1], this.lifebar.lifes[2],],
                    alpha: 0, duration: 2000,
                    onComplete: () => {
                        let end = this.add.image(540, -500, "end-panel");
                        this.tweens.add({
                            targets: end, y: 205,
                            onComplete: () => {
                                this.time.delayedCall(5000, () => {
                                    // fade screen
                                    this.cameras.main.fadeOut(500);

                                    // after fade animation start menu scene
                                    this.time.delayedCall(500, this.scene.start,
                                        ["LoadScene"], this.scene);
                                }, [], this)
                            }
                        })
                    }
                });
            }
        })
    }

    private loose(): void {
        this.state = GameState.LOOSE;
        this.ship.visible = false;
        let home = this.add.image(370, 960, "home-button");
        home.setInteractive();
        home.on("pointerover", () => home.setScale(1.1));
        home.on("pointerout", () => home.setScale(1));

        home.once("pointerdown", () => {
            home.disableInteractive();
            home.setScale(1);
            this.loadMenu();
        });


        let restart = this.add.image(700, 960, "restart-button");
        restart.setInteractive();
        restart.on("pointerover", () => restart.setScale(1.1));
        restart.on("pointerout", () => restart.setScale(1));

        restart.once("pointerdown", () => {
            restart.disableInteractive();
            restart.setScale(1);
            this.restart();
        });
    }


    private loadTutorial(): void {
        let tutorial = this.add.image(540, 960, "tutorial");
        this.time.delayedCall(3000, () => {
            this.tweens.add({
                targets: tutorial,
                alpha: 0,
                onComplete: () => {
                    this.state = GameState.PLAY;
                }
            })
        }, [], this)
    }

    private loadMenu() {
        // fade screen
        this.cameras.main.fadeOut(500);

        // after fade animation start menu scene
        this.time.delayedCall(500, this.scene.start,
            ["LoadScene"], this.scene);
    }

    private restart() {
        // fade screen
        this.cameras.main.fadeOut(500);

        // after fade animation start menu scene
        this.time.delayedCall(500, this.scene.start,
            ["GameScene"], this.scene);
    }
}
/// <reference path='../phaser.d.ts'/>

export class LoadScene extends Phaser.Scene {
    constructor() {
        super({
            key: "LoadScene"
        });
    }

    preload(): void {
        // setup prefix for loading assets
        this.load.setBaseURL('./assets/');

        let background = this.load.image("background",
            "sprites/background.png");

        // progress bar
        this.add.graphics()
            .fillStyle(0x000000, 0.5)
            .fillRect(0, 0, 1080, 50)
            .generateTexture('loading-bar', 1080, 50)
            .clear()
            .fillStyle(0x9c9c9c, 0.5)
            .fillRect(0, 0, 1, 50)
            .generateTexture('progress-bar', 1, 50)
            .destroy();
    }

    create(): void {
        // set the background and create loading bar
        this.add.image(0, 0, "background").setOrigin(0);

        let loadingBar = this.add.image(0, 1920, 'loading-bar')
            .setOrigin(0, 1);
        let progressBar = this.add.image(0, 1920,
            'progress-bar').setOrigin(0, 1);

        // pass value to change the loading bar fill
        this.load.on("progress", (value) =>
            progressBar.setScale(1920 * value, 1)
        );

        this.load.on("complete", () => {
            // after loading all assets create the animations here
            // so its only happen once
            // this.anims.create({
            //     key: '', frameRate: 8, repeat: 0, yoyo: true,
            //     frames: this.anims.generateFrameNumbers('', { start: 1, end: 8 })
            // });
            this.anims.create({
                key: 'flames',
                frames: this.anims.generateFrameNumbers('flames-0', {
                    start: 0, end: 7
                }),

                repeat: -1,
                frameRate: 30
            });

            this.add.tween({
                targets: [progressBar, loadingBar],
                alpha: 0, duration: 500
            })

            let start = this.add.image(540, 2200, "start-button");
            this.add.tween({
                targets: start, y: 1510, ease: "Quart.easeOut"
            });

            start.setInteractive();
            start.on("pointerover", () => start.setScale(1.1));
            start.on("pointerout", () => start.setScale(1));

            start.once("pointerdown", () => {
                start.disableInteractive();
                start.setScale(1);
                this.loadGame();
            });

            let options = this.add.image(955, 2200, "options-button");
            this.add.tween({
                targets: options, y: 1766, ease: "Quart.easeOut"
            });

            options.setInteractive();
            options.on("pointerover", () => options.setScale(1.1));
            options.on("pointerout", () => options.setScale(1));

            options.on("pointerdown", () => {
                this.tweens.add({
                    targets: this.cameras.main,
                    scrollY: 1920,
                    ease: "Cubic.easeInOut"
                })
            });

            let back = this.add.image(370, 1920 + 960, "home-button");
            back.setInteractive();
            back.on("pointerover", () => back.setScale(1.1));
            back.on("pointerout", () => back.setScale(1));

            back.on("pointerdown", () => {
                this.tweens.add({
                    targets: this.cameras.main,
                    scrollY: 0,
                    ease: "Cubic.easeInOut"
                })
            });

            let sound = this.add.image(700, 1920 + 960, "sound-on");
            sound.setInteractive();
            sound.on("pointerover", () => sound.setScale(1.1));
            sound.on("pointerout", () => sound.setScale(1));

            sound.on("pointerdown", () => {
            });
        }, this);

        // ui
        this.load.image("start-button", "sprites/start-button.png");

        this.load.image("progress-back", "sprites/progress-back.png");
        this.load.image("barra", "sprites/barra.png");

        this.load.image("life-back", "sprites/life-back.png");

        this.load.image("tutorial", "sprites/tutorial.png");

        this.load.image("end-panel", "sprites/end-panel.png");

        this.load.image("restart-button", "sprites/restart-button.png");
        this.load.image("home-button", "sprites/home-button.png");

        this.load.image("options-button", "sprites/options-button.png");

        this.load.image("sound-on", "sprites/sound-off.png");


        // ship
        this.load.image("ship", "sprites/ship.png");

        // map
        for (let i = 0; i < 4; i++) {
            this.load.image("pipe-" + i, "sprites/pipe-" + i + ".png");
            this.load.image("pipe-alpha-" + i, "sprites/pipe-alpha-" + i + ".png");
        }

        this.load.image("end", "sprites/end.png");

        // audio
        this.load.image("pipe-alpha-1", "sprites/pipe-alpha-1.png");

        this.load.spritesheet('flames-0', 'sprites/flames.png', { frameHeight: 143, frameWidth: 144 });
        this.load.image("f0", "sprites/f0.png");
        // this.load.image("f1", "sprites/f1.png");
        // this.load.image("f2", "sprites/f2.png");
        // this.load.image("f3", "sprites/f3.png");
        // this.load.image("f4", "sprites/f4.png");
        // this.load.image("f5", "sprites/f5.png");
        // this.load.image("f6", "sprites/f6.png");
        // this.load.image("f7", "sprites/f7.png");

        // generic
        // this.load.spritesheet('blue-anim', 'sprites/blue-anim.png', {
        //     frameWidth: 256, frameHeight: 256
        // });

        this.load.audio('turbine-on', 'audio/turbinaFuncionando.mp3');
        this.load.audio('turbine-off', 'audio/turbinaApagada.mp3');

        // restart load process
        this.load.start();
    }

    private loadGame() {
        // fade screen
        this.cameras.main.fadeOut(500);

        // after fade animation start menu scene
        this.time.delayedCall(500, this.scene.start,
            ["GameScene"], this.scene);
    }
}